(function(window, undefined) {
  var dictionary = {
    "f85875fb-5e23-4dde-ad82-c034f3989f63": "Screen 2",
    "d12245cc-1680-458d-89dd-4f0d7fb22724": "Screen 1",
    "9d35966f-3a26-47a8-8a44-6c7e1b5154fc": "Screen 0",
    "d80d9b6c-225b-48eb-a446-fe5f897a522a": "Screen 1-1",
    "256db694-b915-4c39-b9dc-eba16569bcf8": "Screen 3-3",
    "28cddb59-65df-407e-aa8b-335704490d49": "Screen 3-2",
    "706e5d8b-c8c2-433e-9d3d-e05be409c360": "Screen 3-1",
    "f39803f7-df02-4169-93eb-7547fb8c961a": "Template 1",
    "bb8abf58-f55e-472d-af05-a7d1bb0cc014": "default"
  };

  var uriRE = /^(\/#)?(screens|templates|masters|scenarios)\/(.*)(\.html)?/;
  window.lookUpURL = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, url;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      url = folder + "/" + canvas;
    }
    return url;
  };

  window.lookUpName = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, canvasName;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      canvasName = dictionary[canvas];
    }
    return canvasName;
  };
})(window);